//
//  GalleryCollectionViewController.swift
//  FlickrParties
//
//  Created by Pilipenko Dima on 4/25/15.
//  Copyright (c) 2015 Pilipenko Dima. All rights reserved.
//

import Foundation
import UIKit

final class GalleryCollectionViewController: UICollectionViewController {
    var model: GalleryModelProtocol!
    var delegate: GalleryCollectionDelegate?
    
    private var _imageViewerVC: ImageViewerViewController?
    private var _prevReloadTime: NSTimeInterval?
    
    init() {
        var layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 2
        layout.sectionInset = UIEdgeInsetsMake(10, 5, 10, 5)
        layout.itemSize = CGSizeMake(50, 50)
        layout.scrollDirection = .Horizontal
        
        super.init(collectionViewLayout: layout)
    }
    
    deinit {
        model = nil
        delegate = nil
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView!.registerClass(ImageUICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        collectionView!.alwaysBounceHorizontal = true
    }
    
    // * PROTOCOLS
    // UICollectionViewDataSource
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.dataCount
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! ImageUICollectionViewCell
        cell.backgroundColor = UIColor.darkGrayColor().colorWithAlphaComponent(0.4)
        cell.clearImage()
        
        let cellData = model.getDataAt(indexPath.row)!
        model.requestImage(cellData.thumbnailURI, taskStoringCallback: { value in
            cell.storeTaskAndRun(value)
        }) { value in
            cell.setImage(value)
        }
        
        return cell
    }
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    // UICollectionViewDelegate
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {        
        self._imageViewerVC = ImageViewerViewController()
        _imageViewerVC!.modalPresentationStyle = .OverCurrentContext
        _imageViewerVC!.modalTransitionStyle = .CoverVertical
        
        let cellData = model.getDataAt(indexPath.row)!
        _imageViewerVC!.imageTitle = cellData.imageTitle
        
        model.requestImage(cellData.defaultURI, taskStoringCallback: {[weak self] value in
            self!._imageViewerVC!.storeTaskAndRun(value)
        }) {[weak self] value in
            self!._imageViewerVC!.setImage(value)
        }
        
        presentViewController(_imageViewerVC!, animated: true, completion: nil)
    }
    
    // UIScrollViewDelegate
    override func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        var isPossible = false
        let time = NSDate().timeIntervalSince1970
        if _prevReloadTime == nil || time - _prevReloadTime! > 3 {
            _prevReloadTime = time
            isPossible = true
        }
        
        if isPossible {
            let edge = scrollView.contentOffset.x + CGRectGetWidth(scrollView.bounds)
            let isOver = edge == scrollView.contentSize.width || edge > scrollView.contentSize.width
            if isOver {
                delegate?.requestNextData()
            }
        }
    }
}
