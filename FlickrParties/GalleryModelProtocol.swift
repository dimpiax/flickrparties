//
//  GalleryModelProtocol.swift
//  FlickrParties
//
//  Created by Pilipenko Dima on 4/25/15.
//  Copyright (c) 2015 Pilipenko Dima. All rights reserved.
//

import Foundation
import UIKit

protocol GalleryModelProtocol {
    var title: String { get }
    var systemTabBarItem: UITabBarSystemItem { get }
    
    var dataCount: Int { get }
    
    func setRequester(value: RequesterProtocol)
    
    func nextPage()
    func loadData(completion: Result -> Void)
    func getDataAt(index: Int) -> GalleryDataProtocol?
    
    func requestImage(uri: String, taskStoringCallback: NSURLSessionDataTask -> (), completionCallback: UIImage -> ())
}
