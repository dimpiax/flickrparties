//
//  MainViewController.swift
//  FlickrParties
//
//  Created by Pilipenko Dima on 4/25/15.
//  Copyright (c) 2015 Pilipenko Dima. All rights reserved.
//

import UIKit

final class MainViewController: UIViewController {
    var model: MainModel!
    
    private var _commonGalleryViewController: CommonGalleryViewController!
    private var _nearGalleryViewController: NearGalleryViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self._commonGalleryViewController = CommonGalleryViewController()
        _commonGalleryViewController.model = model.commonGalleryModel
        _commonGalleryViewController.commitInitialProperties()
        
        
        self._nearGalleryViewController = NearGalleryViewController()
        _nearGalleryViewController.model = model.nearGalleryModel
        _nearGalleryViewController.commitInitialProperties()
        
        let tabVC = UITabBarController()
        tabVC.setViewControllers([_commonGalleryViewController, _nearGalleryViewController], animated: true)
        addChildViewController(tabVC)
        view.addSubview(tabVC.view)
    }
}

