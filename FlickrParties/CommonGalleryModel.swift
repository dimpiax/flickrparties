//
//  CommonGalleryModel.swift
//  FlickrParties
//
//  Created by Pilipenko Dima on 4/25/15.
//  Copyright (c) 2015 Pilipenko Dima. All rights reserved.
//

import Foundation
import UIKit

final class CommonGalleryModel<T: GalleryDataProtocol>: GalleryModelProtocol {
    private var _requester: RequesterProtocol?
    
    private var _data: [T]?
    
    var title: String {
        return "All"
    }
    
    var systemTabBarItem: UITabBarSystemItem {
        return .Recents
    }
    
    var dataCount: Int {
        return _data?.count ?? 0
    }
    
    func setRequester(value: RequesterProtocol) {
        _requester = value
    }
    
    func nextPage() {
        _requester!.nextScope()
    }
    
    func loadData(completion: Result -> Void) {
        if var requester = _requester {
            NSURLConnection.sendAsynchronousRequest(requester.urlRequest, queue: NSOperationQueue()) {[weak self] (response, data, error) in
                var parseError: NSError?
                let json: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: &parseError)
                if parseError != nil {
                    completion(.Error(parseError!))
                }
                else {
                    if let HTTPResponse = response as? NSHTTPURLResponse where HTTPResponse.statusCode == 200 {
                        if requester.isValidStructure(json) {
                            if let generator = requester.data as? [AnyObject] {
                                let strongSelf = self!
                                if strongSelf._data == nil { strongSelf._data = [] }
                                for value in generator {
                                    if let mouldedData = requester.mouldData(value) as? T {
                                        strongSelf._data!.append(mouldedData)
                                    }
                                }
                                completion(.Success)
                            }
                            else {
                                completion(.Error(NSError(domain: "Incorrect structure", code: 0, userInfo: nil)))
                            }
                        }
                        else {
                            completion(.Error(NSError(domain: "Incorrect structure", code: 0, userInfo: nil)))
                        }
                    }
                    else {
                        completion(.Error(NSError(domain: "Server successful response not allowed", code: 0, userInfo: nil)))
                    }
                }
            }
        }
        else {
            completion(.Error(NSError(domain: "No requester for operation", code: 0, userInfo: nil)))
        }
    }
    
    func getDataAt(index: Int) -> GalleryDataProtocol? {
        return _data?[index]
    }
    
    private var _sessionConfig: NSURLSessionConfiguration?
    private var _session: NSURLSession?
    
    func requestImage(uri: String, taskStoringCallback: NSURLSessionDataTask -> (), completionCallback: UIImage -> ()) {
        if _sessionConfig == nil {
            _sessionConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
            _sessionConfig!.URLCache = NSURLCache(memoryCapacity: 4*1024*1024, diskCapacity: 20*1024*1024, diskPath: "images/")
        }
        if _session == nil {
            _session = NSURLSession(configuration: _sessionConfig!)
        }
        
        let task = _session!.dataTaskWithURL(NSURL(string: uri)!) { (data, response, error) in
            if error != nil {
                //error
            }
            else {
                if let HTTPResponse = response as? NSHTTPURLResponse where HTTPResponse.statusCode == 200 {
                        NSOperationQueue.mainQueue().addOperationWithBlock {
                        completionCallback(UIImage(data: data)!)
                    }
                }
            }
        }
        taskStoringCallback(task)
    }
}


