//
//  RequesterProtocol.swift
//  FlickrParties
//
//  Created by Pilipenko Dima on 4/25/15.
//  Copyright (c) 2015 Pilipenko Dima. All rights reserved.
//

import Foundation
import UIKit

protocol RequesterProtocol {
    var urlRequest: NSURLRequest { get }
    var data: AnyObject? { get }
    
    mutating func isValidStructure(value: AnyObject?) -> Bool
    
    mutating func nextScope()
    
    func mouldData(value: AnyObject) -> Any
}