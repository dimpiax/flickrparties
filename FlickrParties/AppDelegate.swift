//
//  AppDelegate.swift
//  FlickrParties
//
//  Created by Pilipenko Dima on 4/25/15.
//  Copyright (c) 2015 Pilipenko Dima. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var model: MainModel!

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Hi, TryCatch!
        
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window!.makeKeyAndVisible()
        window!.backgroundColor = UIColor.whiteColor()
        
        self.model = MainModel()
        
        let viewController = MainViewController()
        viewController.model = model
        window!.rootViewController = viewController
        
        return true
    }
}

