//
//  ImageUICollectionViewCell.swift
//  FlickrParties
//
//  Created by Pilipenko Dima on 4/26/15.
//  Copyright (c) 2015 Pilipenko Dima. All rights reserved.
//

import Foundation
import UIKit

final class ImageUICollectionViewCell: UICollectionViewCell {
    private var _task: NSURLSessionDataTask?
    
    func setImage(value: UIImage) {
        backgroundView = UIImageView(image: value)
    }
    
    func storeTaskAndRun(value: NSURLSessionDataTask) {
        _task?.cancel()
        
        value.resume()
        _task = value
    }
    
    func clearImage() {
        backgroundView = nil
    }
}