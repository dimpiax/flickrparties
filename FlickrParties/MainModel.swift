//
//  MainModel.swift
//  FlickrParties
//
//  Created by Pilipenko Dima on 4/25/15.
//  Copyright (c) 2015 Pilipenko Dima. All rights reserved.
//

import Foundation

final class MainModel {
    let commonGalleryModel = CommonGalleryModel<ImageData>()
    let nearGalleryModel = NearGalleryModel()
    
    init() {
        commonGalleryModel.setRequester(FlickrRequester())
    }
}



