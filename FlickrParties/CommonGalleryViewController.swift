//
//  CommonGalleryViewController.swift
//  FlickrParties
//
//  Created by Pilipenko Dima on 4/25/15.
//  Copyright (c) 2015 Pilipenko Dima. All rights reserved.
//

import Foundation
import UIKit

final class CommonGalleryViewController: UIBaseViewController, GalleryCollectionDelegate {
    
    private var _collectionViewController: GalleryCollectionViewController!
    
    private var _prevDataCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self._collectionViewController = GalleryCollectionViewController()
        _collectionViewController.model = model
        _collectionViewController.delegate = self
        addChildViewController(_collectionViewController)
        view.addSubview(_collectionViewController.view)
        
        loadCollectionData()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        _collectionViewController.view.frame = CGRectMake(0, topLayoutGuide.length, CGRectGetWidth(view.bounds), CGRectGetHeight(view.bounds)-bottomLayoutGuide.length-topLayoutGuide.length)
    }
    
    func loadCollectionData() {
        model.nextPage()
        model.loadData {[weak self] value in
            let strongSelf = self!
            
            switch value {
                case .Success:
                    var indexPaths = [NSIndexPath]()
                    for index in strongSelf._prevDataCount..<strongSelf.model.dataCount {
                        indexPaths.append(NSIndexPath(forRow: index, inSection: 0))
                    }
                    strongSelf._prevDataCount = strongSelf.model.dataCount
                    
                    NSOperationQueue.mainQueue().addOperationWithBlock {
                        strongSelf._collectionViewController.collectionView!.insertItemsAtIndexPaths(indexPaths)
                    }
                    
                case .Error(let error):
                    println("error: \(error.localizedDescription)")
            }
        }
    }
    
    // * PROTOCOLS
    // GalleryCollectionDelegate
    func requestNextData() {
        loadCollectionData()
    }
}