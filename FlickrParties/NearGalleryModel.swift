//
//  NearGalleryModel.swift
//  FlickrParties
//
//  Created by Pilipenko Dima on 4/25/15.
//  Copyright (c) 2015 Pilipenko Dima. All rights reserved.
//

import Foundation
import UIKit

final class NearGalleryModel: GalleryModelProtocol {
    var title: String {
        return "Near"
    }
    
    var systemTabBarItem: UITabBarSystemItem {
        return .Search
    }
    
    var dataCount: Int {
        return 0
    }
    
    func nextPage() {
        
    }
    
    func loadData(completion: Result -> Void) {
        completion(.Success)
    }
    
    func setRequester(value: RequesterProtocol) {
        // not implemented
    }
    
    func getDataAt(index: Int) -> GalleryDataProtocol? {
        return nil
    }
    
    func requestImage(uri: String, taskStoringCallback: NSURLSessionDataTask -> (), completionCallback: UIImage -> ()) {
        // not implemented
    }
}
