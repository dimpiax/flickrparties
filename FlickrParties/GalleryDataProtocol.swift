//
//  GalleryDataProtocol.swift
//  FlickrParties
//
//  Created by Pilipenko Dima on 4/26/15.
//  Copyright (c) 2015 Pilipenko Dima. All rights reserved.
//

import Foundation

protocol GalleryDataProtocol {
    var imageTitle: String { get }
    
    var thumbnailURI: String { get }
    var defaultURI: String { get }
}