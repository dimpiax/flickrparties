//
//  ImageData.swift
//  FlickrParties
//
//  Created by Pilipenko Dima on 4/26/15.
//  Copyright (c) 2015 Pilipenko Dima. All rights reserved.
//

import Foundation

struct ImageData: GalleryDataProtocol {
    let farm: String
    let id: String
    let owner: String
    let secret: String
    let server: String
    let title: String
    
    var imageTitle: String {
        return title
    }
    
    var thumbnailURI: String {
        return "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)_t.jpg"
    }
    
    var defaultURI: String {
        return "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)_z.jpg"
    }
}