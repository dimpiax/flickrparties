//
//  Enums.swift
//  FlickrParties
//
//  Created by Pilipenko Dima on 4/26/15.
//  Copyright (c) 2015 Pilipenko Dima. All rights reserved.
//

import Foundation
import UIKit

enum Result {
    case Success, Error(NSError)
}