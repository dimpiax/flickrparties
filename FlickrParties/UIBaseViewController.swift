//
//  UIBaseViewController.swift
//  FlickrParties
//
//  Created by Pilipenko Dima on 4/25/15.
//  Copyright (c) 2015 Pilipenko Dima. All rights reserved.
//

import Foundation
import UIKit

class UIBaseViewController: UIViewController {
    var model: GalleryModelProtocol!
    
    func commitInitialProperties() {
        title = model.title
        
        tabBarItem = UITabBarItem(tabBarSystemItem: model.systemTabBarItem, tag: 0)
    }
}