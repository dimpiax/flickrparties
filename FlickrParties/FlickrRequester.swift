//
//  FlickrRequester.swift
//  FlickrParties
//
//  Created by Pilipenko Dima on 4/26/15.
//  Copyright (c) 2015 Pilipenko Dima. All rights reserved.
//

import Foundation

struct FlickrRequester: RequesterProtocol {
    private let _host = "https://api.flickr.com/services/rest/"
    private let _apiKey = "7d7b55e0113f92bde67d7dea8c19c536"
    private let _method = "flickr.photos.search"
    
    private let _format = "json"
    private let _tags = "party"
    
    private let _perPage = 100
    private var _currentPage = 0
    
    private var _data: AnyObject?
    
    var urlRequest: NSURLRequest {
        return NSURLRequest(URL: NSURL(string: "\(_host)?api_key=\(_apiKey)&method=\(_method)&tags=\(_tags)&per_page=\(_perPage)&page=\(_currentPage)&format=\(_format)&nojsoncallback=1")!)
    }
    
    var data: AnyObject? {
        return _data
    }
    
    mutating func isValidStructure(value: AnyObject?) -> Bool {
        if let data: AnyObject = value, photos = data["photos"] as? [String: AnyObject], photosData = photos["photo"] as? [AnyObject] {
            _data = photosData
            return true
        }
        
        return false
    }
    
    mutating func nextScope() {
        _currentPage++
    }
    
    func mouldData(value: AnyObject) -> Any {
        return ImageData(farm: String(value["farm"] as! Int), id: value["id"] as! String, owner: value["owner"] as! String, secret: value["secret"] as! String, server: value["server"] as! String, title: value["title"] as! String)
    }
}