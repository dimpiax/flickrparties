//
//  ImageViewerViewController.swift
//  FlickrParties
//
//  Created by Pilipenko Dima on 4/26/15.
//  Copyright (c) 2015 Pilipenko Dima. All rights reserved.
//

import Foundation
import UIKit

final class ImageViewerViewController: UIViewController {
    var imageTitle: String?
    
    private var _imageView: UIImageView!
    private var _closeButton: UIButton!
    private var _titleField: UILabel!
    
    private var _task: NSURLSessionDataTask?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.9)
        
        self._imageView = UIImageView()
        view.addSubview(_imageView)
        
        self._closeButton = UIButton.buttonWithType(.System) as! UIButton
        _closeButton.setTitle("Close", forState: .Normal)
        _closeButton.sizeToFit()
        _closeButton.addTarget(self, action: "closeButtonDidTapped", forControlEvents: .TouchUpInside)
        view.addSubview(_closeButton)
        
        self._titleField = UILabel()
        _titleField.font = UIFont(name: "HelveticaNeue", size: 15)
        _titleField.numberOfLines = 0
        _titleField.lineBreakMode = NSLineBreakMode.ByWordWrapping
        _titleField.textAlignment = .Center
        _titleField.backgroundColor = UIColor.blackColor()
        _titleField.textColor = UIColor.whiteColor()
        _titleField.text = imageTitle
        _titleField.sizeToFit()
        view.addSubview(_titleField)
        
        updateTextRectangle()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        _closeButton.frame.origin = CGPointMake(CGRectGetWidth(view.bounds)-CGRectGetWidth(_closeButton.bounds)-10, topLayoutGuide.length+10)
        
        updateImageRectangle()
        updateTextRectangle()
    }
    
    func setImage(value: UIImage?) {
        _imageView.image = value
        if value != nil {
            _imageView.alpha = 0
            UIView.animateWithDuration(0.7) {[weak self] in
                self!._imageView.alpha = 1
            }
            
            _imageView.sizeToFit()
            updateImageRectangle()
        }
    }
    
    func storeTaskAndRun(value: NSURLSessionDataTask) {
        _task?.cancel()
        
        value.resume()
        _task = value
    }
    
    func clearImage() {
        setImage(nil)
    }
    
    // * ACTIONS
    func closeButtonDidTapped() {
        _task?.cancel()
        dismissViewControllerAnimated(true) {[weak self] in
            self!.setImage(nil)
        }
    }
    
    // *** PRIVATE METHODS
    private func updateTextRectangle() {
        _titleField.sizeToFit()
        _titleField.frame = CGRectMake(0, CGRectGetHeight(view.bounds)-CGRectGetHeight(_titleField.bounds)-10, CGRectGetWidth(view.bounds), CGRectGetHeight(_titleField.bounds))
    }
    
    private func updateImageRectangle() {
        if _imageView.image != nil {
            let scale = min(CGRectGetWidth(view.bounds)/CGRectGetWidth(_imageView.bounds), CGRectGetHeight(view.bounds)/CGRectGetHeight(_imageView.bounds))
            
            _imageView.transform = CGAffineTransformMakeScale(scale, scale)
            
            let y = max(topLayoutGuide.length, CGRectGetMidY(view.bounds)-CGRectGetMidY(_imageView.bounds)*scale)
            _imageView.frame.origin = CGPointMake(0, y)
        }
    }
}